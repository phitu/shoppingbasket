package rbc.shoppingbasket;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;

public class BasketTest {

	private Basket basket = new Basket();
	
	@Test
	public void calcBasket() {
		basket.getBasket().add(new Lemon());
		basket.getBasket().add(new Lemon());
		basket.getBasket().add(new Banana());
		basket.getBasket().add(new Peach());
		assertThat(basket.calculateBasket(), is(new BigDecimal(3.2).setScale(2, BigDecimal.ROUND_HALF_EVEN)));
	}
	
	@Test
	public void calcBasketNoItems() {
		assertThat(basket.calculateBasket(), is(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_EVEN)));
	}
}
