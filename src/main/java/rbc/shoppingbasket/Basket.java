package rbc.shoppingbasket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Basket {

	private List<Item> basket = new ArrayList<>();

	public List<Item> getBasket() {
		return basket;
	}

	public BigDecimal calculateBasket() {
		BigDecimal totalPrice = getBasket().stream().map(a->a.getPrice()).reduce(BigDecimal.ZERO, BigDecimal::add);
		return totalPrice.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}
	
	public void displayItems() {
		for(Item item : getBasket()) {
			System.out.println(item);
		}
	}
}
