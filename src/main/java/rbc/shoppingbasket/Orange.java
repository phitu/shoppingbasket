package rbc.shoppingbasket;

import java.math.BigDecimal;

public class Orange extends Item {

	private static final String ORANGE = "Orange";

	public Orange() {
		super(ORANGE, new BigDecimal(1));
	}

}
