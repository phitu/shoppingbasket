package rbc.shoppingbasket;

import java.math.BigDecimal;

public class Apple extends Item {

	private static final String APPLE = "Apple";

	public Apple() {
		super(APPLE, new BigDecimal(0.1));
	}

}
