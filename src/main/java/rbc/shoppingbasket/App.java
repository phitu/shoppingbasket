package rbc.shoppingbasket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App 
{
	private Basket basket = new Basket();
	private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	private void inputItem() throws IOException {
		String menu = br.readLine();
		switch(menu) {
			case "1" : basket.getBasket().add(new Apple());
				inputItem();
				break;
			case "2" : basket.getBasket().add(new Banana());
				inputItem();
				break;
			case "3" : basket.getBasket().add(new Lemon());
				inputItem();
				break;
			case "4" : basket.getBasket().add(new Orange());
				inputItem();
				break;
			case "5" : basket.getBasket().add(new Peach());
				inputItem();
				break;
			case "0" : basket.displayItems();
				System.out.println("Total Price-" + basket.calculateBasket());
				break;
			default : System.out.println("Command not recognised");
				inputItem();
				break;
		}
	}
	
	public void displayItems() throws IOException {		
		System.out.println("Enter 1 to buy apple");
		System.out.println("Enter 2 to buy banana");
		System.out.println("Enter 3 to buy lemon");
		System.out.println("Enter 4 to buy orange");
		System.out.println("Enter 5 to buy peach");
		System.out.println("Enter 0 to pay");
		inputItem();
	}
	 
    public static void main( String[] args )
    {
		System.out.println("Shopping Program");
		try {
			new App().displayItems();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
