package rbc.shoppingbasket;

import java.math.BigDecimal;

public class Peach extends Item {

	private static final String PEACH = "Peach";

	public Peach() {
		super(PEACH, new BigDecimal(2));
	}

}
