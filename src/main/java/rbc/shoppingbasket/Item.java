package rbc.shoppingbasket;

import java.math.BigDecimal;

public abstract class Item {

	private String itemName;
	
	private BigDecimal price;
	
	public Item(String name, BigDecimal price) {
		this.setItemName(name);
		this.setPrice(price);
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return itemName + " Price-" + price.setScale(2, BigDecimal.ROUND_HALF_EVEN);		
	}
}
