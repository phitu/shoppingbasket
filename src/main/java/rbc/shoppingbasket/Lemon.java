package rbc.shoppingbasket;

import java.math.BigDecimal;

public class Lemon extends Item {

	private static final String LEMON = "Lemon";

	public Lemon() {
		super(LEMON, new BigDecimal(0.5));
	}

}
