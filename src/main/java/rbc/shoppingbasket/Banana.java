package rbc.shoppingbasket;

import java.math.BigDecimal;

public class Banana extends Item {

	private static final String BANANA = "Banana";

	public Banana() {
		super(BANANA, new BigDecimal(0.2));
	}

}
